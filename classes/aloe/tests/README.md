# tests example using aloe framework

Project that allows tests all endpoints of one API

## Prerequirements

Python<br />
aloe (sudo pip3 install aloe)

## Running tests

In the current directory execute:

`aloe tests/features`

or if you want execute specific feature execute

`aloe tests/features/<<NameOfTheFeature>>.feature`


## Documentation

https://aloe.readthedocs.io/en/latest/<br />
https://buildmedia.readthedocs.org/media/pdf/aloe/latest/aloe.pdf